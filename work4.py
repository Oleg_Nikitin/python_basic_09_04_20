# Пользователь вводит целое положительное число. Найдите самую большую цифру в числе.
# Для решения используйте цикл while и арифметические операции.


numbers = input('Введите целое положительное число: ') # Ввод числа
i = 0 # Создание индекса

the_biggest_number = int(numbers[0]) # Создание переменной для поиска самой большой цифры в числе

# Поиск самой большой цифры в числе
while i <  len(numbers):
    if the_biggest_number < int(numbers[i]):
        the_biggest_number = int(numbers[i])
    i += 1

print(the_biggest_number)





#print(the_biggest_number)



