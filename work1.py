# Поработайте с переменными, создайте несколько, выведите на экран, запросите у пользователя
# несколько чисел и строк и сохраните в переменные, выведите на экран.

# Создание переменных

var_1 = 'Hello World' # str строка
var_2 = 3 # int целое число
var_3 = 3.34 # float дробное число
var_4 = True # переменная bool
var_none = None # None
home_number = input('Введите номер дома: ') # str строка
age = input('Ваш возраст: ') # str строка
age_int = int(age) # преобразование в целое число , без "защиты от дурака"


# Вывод переменных на экран

print(var_1)
print(var_2)
print(var_3)
print(var_4)
print(var_none)
print(home_number)
print(age)
print(type(age))
print(age_int)
print(type(age_int))
