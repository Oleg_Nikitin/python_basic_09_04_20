#  Пользователь вводит время в секундах. Переведите время в часы, минуты и секунды и выведите в формате чч:мм:сс.
#  Используйте форматирование строк.

input_of_time = input('введите время в секундах: ') # ввод времени без "защиты от дурака"

seconds = int(input_of_time) % 60 # считаем секунды
minutes = int(input_of_time) // 60 # количество полных минут
hours = minutes //60 # количество полных часов
minutes = minutes - hours * 60 # оставшиеся минуты

# Вывод в формате чч:мм:сс
print(f"{hours:>02}:{minutes:>02}:{seconds:>02}")